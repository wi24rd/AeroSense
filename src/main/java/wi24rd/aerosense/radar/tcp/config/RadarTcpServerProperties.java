package wi24rd.aerosense.radar.tcp.config;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "radar.tcp")
@Data
public class RadarTcpServerProperties implements Serializable {


    private boolean enable;

    private String addressMap = "HashMap";

    private String host = "0.0.0.0";

    private int port = 8899;

    private long idleTimeout = 90000;


    private List<String> preferredNetworks = new ArrayList<>();

    private List<String> ignoredInterfaces = new ArrayList<>();

    private boolean preferHostnameOverIP;

    private boolean useOnlySiteLocalInterface;
}
