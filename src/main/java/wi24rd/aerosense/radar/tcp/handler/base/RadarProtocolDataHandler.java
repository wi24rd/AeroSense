package wi24rd.aerosense.radar.tcp.handler.base;

import wi24rd.aerosense.radar.tcp.protocol.FunctionEnum;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolData;
import com.alipay.remoting.exception.RemotingException;

import java.util.Set;


public interface RadarProtocolDataHandler {

    Object process(RadarProtocolData protocolData) throws RemotingException, InterruptedException;

    Set<FunctionEnum> interests();
}
