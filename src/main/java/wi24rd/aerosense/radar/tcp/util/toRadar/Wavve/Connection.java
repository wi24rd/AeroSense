package wi24rd.aerosense.radar.tcp.util.toRadar.Wavve;
import com.alipay.remoting.exception.RemotingException;
import wi24rd.aerosense.radar.tcp.protocol.FunctionEnum;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolConsts;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolData;
import wi24rd.aerosense.radar.tcp.server.RadarTcpServer;
import wi24rd.aerosense.radar.tcp.util.toRadar.RequestRadarUtil;
import wi24rd.aerosense.radar.tcp.util.ByteUtil;
import org.springframework.stereotype.Component;

@Component
public class Connection extends RequestRadarUtil {

    public Connection(RadarTcpServer radarTcpServer) {
        super(radarTcpServer);
    }

    public static boolean getFirstRequest(String radarId) throws RemotingException {
        RadarProtocolData radarProtocolData = RadarProtocolData.newInstance(radarId,
                FunctionEnum.WAVVE_REQUEST_FIRST_GET, ByteUtil.intToByteBig(0));
        RadarProtocolData retObj = invokeToRadar(radarProtocolData);
        return  ByteUtil.byte4ToInt(retObj.getData()) == RadarProtocolConsts.RET_SUCCESS;
    }

}