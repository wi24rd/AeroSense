package wi24rd.aerosense.radar.tcp.util.toRadar.Wavve;

import wi24rd.aerosense.radar.tcp.protocol.FunctionEnum;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolData;
import wi24rd.aerosense.radar.tcp.server.RadarTcpServer;
import wi24rd.aerosense.radar.tcp.util.toRadar.RequestRadarUtil;
import wi24rd.aerosense.radar.tcp.util.ByteUtil;
import com.alipay.remoting.exception.RemotingException;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
public class ID extends RequestRadarUtil {

    public ID(RadarTcpServer radarTcpServer) {
        super(radarTcpServer);
    }


    public static String get(String radarId) throws RemotingException {
        RadarProtocolData radarProtocolData = RadarProtocolData.newInstance(radarId,
                FunctionEnum.WAVVE_UUID_GET, ByteUtil.intToByteBig(0));
        RadarProtocolData retObj = invokeToRadar(radarProtocolData);
        return new String(retObj.getData(), StandardCharsets.UTF_8);
    }

}
