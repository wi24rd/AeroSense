package wi24rd.aerosense.radar.tcp.util.toRadar.Wavve;

import wi24rd.aerosense.radar.tcp.protocol.FunctionEnum;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolConsts;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolData;
import wi24rd.aerosense.radar.tcp.server.RadarTcpServer;
import wi24rd.aerosense.radar.tcp.util.toRadar.RequestRadarUtil;
import wi24rd.aerosense.radar.tcp.util.ByteUtil;
import com.alipay.remoting.exception.RemotingException;
import org.springframework.stereotype.Component;

@Component
public class Calibration extends RequestRadarUtil {

    public Calibration(RadarTcpServer radarTcpServer) {
        super(radarTcpServer);
    }

    public static boolean on(String radarId) throws RemotingException {
        RadarProtocolData radarProtocolData = RadarProtocolData.newInstance(radarId,
                FunctionEnum.WAVVE_CALIBRATION_ON, ByteUtil.intToByteBig(0));
        RadarProtocolData retObj = invokeToRadar(radarProtocolData);
        return ByteUtil.byte4ToInt(retObj.getData()) == RadarProtocolConsts.RET_SUCCESS;
    }

}
