package wi24rd.aerosense.radar.tcp.util.toRadar.Wavve;

import com.alipay.remoting.exception.RemotingException;
import wi24rd.aerosense.radar.tcp.protocol.FunctionEnum;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolConsts;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolData;
import wi24rd.aerosense.radar.tcp.server.RadarTcpServer;
import wi24rd.aerosense.radar.tcp.util.toRadar.RequestRadarUtil;
import wi24rd.aerosense.radar.tcp.util.ByteUtil;

public class Threshold extends RequestRadarUtil {
    public Threshold(RadarTcpServer radarTcpServer) {
        super(radarTcpServer);
    }
    public static boolean setTimeBedOutLong(String radarId, int time) throws RemotingException {
        if (time < 0) {
            throw new IllegalArgumentException("TimeBedOutLong invalid : " + time);
        }

        RadarProtocolData radarProtocolData = RadarProtocolData.newInstance(radarId,
                FunctionEnum.WAVVE_THRESHOLD_TIME_BED_OUT_LONG_SET, ByteUtil.intToByteBig(time));
        RadarProtocolData retObj = invokeToRadar(radarProtocolData);
        return ByteUtil.byte4ToInt(retObj.getData()) == RadarProtocolConsts.RET_SUCCESS;
    }
    public static int getTimeBedOutLong(String radarId) throws RemotingException {

        RadarProtocolData radarProtocolData = RadarProtocolData.newInstance(radarId,
                FunctionEnum.WAVVE_THRESHOLD_TIME_BED_OUT_LONG_GET, ByteUtil.intToByteBig(0));
        RadarProtocolData retObj = invokeToRadar(radarProtocolData);
        return ByteUtil.bytes2IntBig(retObj.getData());
    }
}
