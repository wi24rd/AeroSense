package wi24rd.aerosense.radar.tcp.service.from.Wavve;


import wi24rd.aerosense.radar.tcp.handler.base.RadarProtocolDataHandler;
import wi24rd.aerosense.radar.tcp.protocol.FunctionEnum;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolData;
import com.alipay.remoting.exception.RemotingException;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import wi24rd.aerosense.radar.tcp.util.toRadar.Wavve.Calibration;
import wi24rd.aerosense.radar.tcp.util.toRadar.Wavve.ID;
import wi24rd.aerosense.radar.tcp.util.toRadar.Wavve.Threshold;

import java.util.Set;

@Log
@Service
public class Report28 implements RadarProtocolDataHandler {

    @Override
    public Object process(RadarProtocolData protocolData) throws RemotingException, InterruptedException {
        java.nio.ByteBuffer bb=java.nio.ByteBuffer.wrap(protocolData.getData()) ;
        log.info("Resp rate: "+ bb.getFloat());
        log.info("Resp curve: "+ bb.getFloat());
        log.info("Heart rate BPM: "+bb.getFloat());
        log.info("Heart rate curve: "+bb.getFloat());
        log.info("Target distance: "+bb.getFloat());
        log.info("Signal strength of the target: "+bb.getFloat());
        log.info("State: "+ bb.getInt());
        //log.info("Setting threshold for out of bed: "+ Threshold.setTimeBedOutLong(protocolData.getRadarId(),1));

        log.info("Threshold for out of bed: "+ Threshold.getTimeBedOutLong(protocolData.getRadarId()));


        if(Calibration.on(protocolData.getRadarId())){
            log.info("Calibration On Successfully");
        }

        return null;
    }

    @Override
    public Set<FunctionEnum> interests() {
        return java.util.EnumSet.of(FunctionEnum.WAVVE_HANDLE_REPORT_28);
    }
}


