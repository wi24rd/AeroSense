package wi24rd.aerosense.radar.tcp.service.from.Wavve;

import wi24rd.aerosense.radar.tcp.handler.base.RadarProtocolDataHandler;
import wi24rd.aerosense.radar.tcp.protocol.FunctionEnum;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolData;
import com.alipay.remoting.exception.RemotingException;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.Set;
@Log
@Service
public class EnergyMove implements RadarProtocolDataHandler {
    @Override
    public Object process(RadarProtocolData protocolData) throws RemotingException, InterruptedException {
        java.nio.ByteBuffer bb=java.nio.ByteBuffer.wrap(protocolData.getData()) ;
        log.info("Accumulated energy for every body movement: "+ bb.getFloat());
        return null;
    }

    @Override
    public Set<FunctionEnum> interests() {
        return java.util.EnumSet.of(FunctionEnum.WAVVE_ENERGY_MOVE);
    }
}
