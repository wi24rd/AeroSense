package wi24rd.aerosense.radar.tcp.service.from;

import wi24rd.aerosense.radar.tcp.protocol.FunctionEnum;
import wi24rd.aerosense.radar.tcp.handler.base.RadarProtocolDataHandler;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolData;
import wi24rd.aerosense.radar.tcp.util.ByteUtil;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import wi24rd.aerosense.radar.tcp.server.RadarTcpServer;

import java.util.Set;



@Log
@Service
public class CreateConnectionHandler implements RadarProtocolDataHandler {
    @Override
    public Object process(RadarProtocolData protocolData) {
        RadarProtocolData radarProtocolData = new RadarProtocolData();
        radarProtocolData.setFunction(FunctionEnum.createConnection);
        radarProtocolData.setData(ByteUtil.intToByteBig(1));


        log.info("\n returning connect id: "+protocolData.getRadarId()+" type: "+protocolData.getRadarType()+"\n");
        log.info("\n radar online count "+RadarTcpServer.getOnlineRadarCount() +"\n");
        log.info("\n radar online list "+RadarTcpServer.getOnlineRadarList() +"\n");
        log.info("\n radar address "+RadarTcpServer.getRadarAddress(protocolData.getRadarId()) +"\n");

        return radarProtocolData;
    }

    @Override
    public Set<FunctionEnum> interests() {
        return java.util.EnumSet.of(FunctionEnum.createConnection);
        //return Sets.newHashSet(FunctionEnum.createConnection);
    }
}
