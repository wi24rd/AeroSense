package wi24rd.aerosense.radar.tcp.service.from.Assure;

import wi24rd.aerosense.radar.tcp.protocol.FunctionEnum;
import com.alipay.remoting.exception.RemotingException;
import com.google.common.collect.Sets;
import wi24rd.aerosense.radar.tcp.handler.base.RadarProtocolDataHandler;
import wi24rd.aerosense.radar.tcp.protocol.RadarProtocolData;
import wi24rd.aerosense.radar.tcp.util.ByteUtil;
import lombok.extern.java.Log;
import org.springframework.stereotype.Service;

import java.util.Set;

@Log
@Service
public class InvadeHandler  implements RadarProtocolDataHandler {

    @Override
    public Object process(RadarProtocolData protocolData) throws RemotingException, InterruptedException {
        //TODO process the invade alarm
        
        log.info("invade alarm");
        log.info("process the invade alarm you want to");
        RadarProtocolData radarProtocolData = new RadarProtocolData();
        radarProtocolData.setFunction(FunctionEnum.Invade);
        radarProtocolData.setData(ByteUtil.intToByteBig(1));
        return radarProtocolData;
    }

    @Override
    public Set<FunctionEnum> interests() {
        return Sets.newHashSet(FunctionEnum.Invade);
    }
}
