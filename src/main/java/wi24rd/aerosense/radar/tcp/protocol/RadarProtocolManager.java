
package wi24rd.aerosense.radar.tcp.protocol;

import com.alipay.remoting.ProtocolManager;


public class RadarProtocolManager {
    /**

     */
    public static void initProtocols() {
        ProtocolManager.registerProtocol(new AssureProtocol(), AssureProtocol.PROTOCOL_CODE);
        ProtocolManager.registerProtocol(new WavveProtocol(), WavveProtocol.PROTOCOL_CODE);
    }
}
