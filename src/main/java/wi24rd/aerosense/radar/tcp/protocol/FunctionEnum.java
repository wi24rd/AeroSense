
package wi24rd.aerosense.radar.tcp.protocol;

import java.util.Arrays;



public enum FunctionEnum {

    UNDEFINED(-1),

    ERROR(-2),

    createConnection(0x0001),

    SetInstallHeight(0x0002),

    GetInstallHeight(0x0003),

    SetFallReportTimer(0x0004),

    GetFallReportTimer(0x0005),

    SetWorkingRange(0x0006),

    GetWorkingRange(0x0007),

    FallDetect(0x0009),

    Invade(0x000a),

    ASSURE_REPORT_HEAT_MAP(0x000b),

    SetHeatMapEnable(0x000c),

    GetHeatMapEnable(0x000d),

    SetInvadeEnable(0x000e),

    GetInvadeEnable(0x000f),

    PositionStudy(0x0010),

    WAVVE_HANDLE_REPORT_28(0x03e8),

    WAVVE_RANGE_DETECTION_MAX_SET(0x03eb),

    WAVVE_RANGE_DETECTION_MAX_GET(0x03ec),
    WAVVE_CALIBRATION_ON(0x03ed),
    WAVVE_THRESHOLD_TIME_BED_OUT_LONG_SET(0x0404),
    WAVVE_THRESHOLD_TIME_BED_OUT_LONG_GET(0x0405),
    WAVVE_ALERT_BED_OUT_LONG(0x0406),
    WAVVE_ALERT_BED_TURN(0x040c),
    WAVVE_UUID_GET(0x0410),
    WAVVE_REQUEST_FIRST_GET(0x0411),




    WAVVE_ENERGY_MOVE(0x040f),
    WAVVE_ZIGBEE_RELAY(0x0413),



    notifyUpdate(0x0021), issueFirmware(0x0022), updateResult(0x0023);

    private final short function;

    FunctionEnum(int function) {
        this.function = (short) function;
    }

    public static FunctionEnum from(short function) {
        return Arrays.stream(FunctionEnum.values())
                .filter(f -> f.getFunction() == function)
                .findFirst()
                .orElse(UNDEFINED);
    }

    public short getFunction() {
        return function;
    }
}
