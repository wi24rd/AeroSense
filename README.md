AeroSense Java SDK
======================
AeroSense Java SDK is a Spring Boot application, this application will listen on port 8899 and accept a TCP socket connection.
Compiling this project requires java8、 Maven3 or later, or you can import to the IDE then run it.

**!!! important !!!**
You must turn off the firewall. The firewall will block the connection.

## Running the project from command line
Run `mvn clean install spring-boot:run` in the project root directory. The server will be started at port 8899.

## Running the project from your IDE
Navigate to the `Application` class and run it as a Java application.